#! /bin/bash

run_home=$PWD
ansible_inv_file="$run_home/ansible/hosts"
key_file="$run_home/ansible/private_key"

# check that all the needed software installed aws, aws-iam-authenticator, kubectl, ansible
for i in 'aws' 'aws-iam-authenticator' 'kubectl' 'ansible' 'docker' 'jq';
do
   if ! [ -x "$(command -v $i)" ];
   then
     echo "$i is not installed on the machine, exiting";
     exit 1
  fi
done

function install_deployment() {
  cd terraform
  echo "Building Terraform"
  terraform init
  terraform plan
  terraform apply -auto-approve

  echo "Configure kubectl to work with the new cluster"
  # config kubectl
  aws eks --region $(terraform output region) update-kubeconfig --name $(terraform output cluster_name)

  # copy the private key
  if [ -e $key_file ]; then rm -f $key_file; fi
  terraform output private_key_pem > $key_file && chmod 400 $key_file

  cd $run_home

  echo "Generating ansible inventory file"
  cd ansible

  if [ -e $ansible_inv_file ]; then rm $ansible_inv_file; fi
  echo "[webservers]" > $ansible_inv_file

  # adding the hosts
  aws ec2 describe-instances --query Reservations[*].Instances[*].[PublicIpAddress] \
      --output text --filter Name=instance-state-name,Values=running \
      >> $ansible_inv_file

  # adding connection settings
  echo ""
  echo "[all:vars]" >> $ansible_inv_file
  echo "ansible_connection=ssh" >> $ansible_inv_file
  echo "ansible_user=ec2-user" >> $ansible_inv_file
  echo "ansible_ssh_private_key_file="$key_file >> $ansible_inv_file

  echo "Install 3rd party software on instances"
  ansible-playbook deployment_playbook.yaml

  cd $run_home

  echo "Deploying pointer app to cluster"
  cd k8s
  kubectl apply -k .


  printf "Waiting to loadBalancer to be available"
  while [ $(kubectl get svc pointer-app -o json | jq -r '.status.loadBalancer.ingress[0].hostname | length') -eq 0 ]
  do
    printf '.'
    sleep 1
  done
  echo ""

  hostname=$(kubectl get svc pointer-app -o json | jq -r '.status.loadBalancer.ingress[0].hostname')

  echo "Pointer App is deployed, goto https://$hostname"

  exit 0
}

function delete_deployed() {
  echo "Deleting pointer app"
  cd k8s
  kubectl delete -k .

  cd $run_home

  echo "Deleting Terraform"
  cd terraform
  terraform destroy -auto-approve

  exit 0
}

function clean() {
  echo "Removing ansible files"
  rm -f $ansible_inv_file
  rm -f $key_file

  echo "Removing terraform state files"
  rm -f terraform/terraform.tfstate*
}

if [ -z "$1" ] || [ "$1" != "delete" ] && [ "$1" != "install" ] && [ "$1" != "clean" ];
then
    echo "no action was selected (install, delete or clean)"
elif [ "$1" == "delete" ];
then
    delete_deployed
elif [ "$1" == "install" ];
then
    install_deployment
else
  clean
fi




