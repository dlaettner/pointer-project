resource "random_pet" "this" {
  length = 2
}

resource "tls_private_key" "this" {
  algorithm = "RSA"
}

module "key_pair" {
  source = "terraform-aws-modules/key-pair/aws"

  key_name   = random_pet.this.id
  public_key = tls_private_key.this.public_key_openssh

  tags = {
    Terraform = "<3"
  }
}