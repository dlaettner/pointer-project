module "eks" {
  source       = "terraform-aws-modules/eks/aws"
  cluster_name = local.cluster_name
  subnets      = module.vpc.public_subnets

  tags = {
    Environment = "training"
    GithubRepo  = "terraform-aws-eks"
    GithubOrg   = "terraform-aws-modules"
  }

  vpc_id = module.vpc.vpc_id

  worker_groups = [
    {
      name                          = "production"
      instance_type                 = "t3a.micro"
      additional_userdata           = "production"
      asg_desired_capacity          = 2
      additional_security_group_ids = [aws_security_group.production.id]
      public_ip                     = true
      key_name                      = module.key_pair.this_key_pair_key_name

    },
    {
      name                          = "staging"
      instance_type                 = "t3a.micro"
      additional_userdata           = "staging"
      additional_security_group_ids = [aws_security_group.staging.id]
      asg_desired_capacity          = 1
      public_ip                     = true
      key_name                      = module.key_pair.this_key_pair_key_name
    },
  ]
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}