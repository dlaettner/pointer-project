## Pointer Project

This project is a POC for deploying a simple app to AWS using terraform and k8s. 

##### Prerequisite
* aws account
* installed software:
    * aws cli
    * aws-iam-authenticator
    * kubectl
    * ansible
    * docker
    * jq

To build it run :
```bash
./run install
```
To delete it run : 
```bash
./run delete
```

#### AWS infrastructure
This project creates 3 instances : 
* 2 with public access
* 1 with private access

#### Pointer
The application that is being deployed is [Pointer](https://bitbucket.org/dlaettner/pointer)
its a sprint management application. (user: admin , pass: q1w2e3r4) 